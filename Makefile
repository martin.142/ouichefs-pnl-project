obj-m += ouichefs.o
ouichefs-objs := fs.o super.o inode.o file.o dir.o

CHECK_PATCH=./checkpatch.pl --no-tree
#KERNELDIR ?= /lib/modules/$(shell uname -r)/build
KERNELDIR ?= ../../LinuxKernelProjet/linux-5.10.17/	

all:
	make -C $(KERNELDIR) M=$(PWD) modules
	cp ouichefs.ko ../../LinuxKernelProjet/pnl/share/

check:
	for f in *.c *.h ; do \
		$(CHECK_PATCH) -f $$f ; \
	done

debug:
	make -C $(KERNELDIR) M=$(PWD) ccflags-y+="-DDEBUG -g" modules

clean:
	make -C $(KERNELDIR) M=$(PWD) clean
	rm -rf *~

.PHONY: all clean
