// SPDX-License-Identifier: GPL-2.0
/*
 * ouiche_fs - a simple educational filesystem for Linux
 *
 * Copyright (C) 2018 Redha Gouicem <redha.gouicem@lip6.fr>
 */

#define pr_fmt(fmt) "ouichefs: " fmt

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/buffer_head.h>
#include <linux/mpage.h>
#include <linux/list.h>
#include <linux/ioctl.h>
#include <linux/file.h>
#include <linux/sysctl.h>
#include <linux/unistd.h>
//#include <unistd.h>


#include "ouichefs.h"
#include "bitmap.h"

/*
 * Map the buffer_head passed in argument with the iblock-th block of the file
 * represented by inode. If the requested block is not allocated and create is
 * true, allocate a new block on disk and map it.
 *
 * MODIFICATIONS
 * Force modified buffer blocks to be written to the disk with sync_dirty_buffer
 */
static int ouichefs_file_get_block(struct inode *inode, sector_t iblock,
				   struct buffer_head *bh_result, int create)
{
	struct super_block *sb = inode->i_sb;
	struct ouichefs_sb_info *sbi = OUICHEFS_SB(sb);
	struct ouichefs_inode_info *ci = OUICHEFS_INODE(inode);
	struct ouichefs_file_index_block *index;
	struct buffer_head *bh_index;
	bool alloc = false;
	int ret = 0, bno;


	/* If block number exceeds filesize, fail */
	if (iblock >= (OUICHEFS_BLOCK_SIZE >> 2) - 1)
		return -EFBIG;

	/* Read index block from disk */
	bh_index = sb_bread(sb, ci->index_block);
	if (!bh_index)
		return -EIO;
	index = (struct ouichefs_file_index_block *)bh_index->b_data;

	/*
	 * Check if iblock is already allocated. If not and create is true,
	 * allocate it. Else, get the physical block number.
	 */
	if (index->blocks[iblock] == 0) {
		if (!create)
			return 0;
		bno = get_free_block(sbi);
		if (!bno) {
			ret = -ENOSPC;
			goto brelse_index;
		}
		index->blocks[iblock] = bno;
		alloc = true;
	} else {
		bno = index->blocks[iblock];
	}

	/* Map the physical block to to the given buffer_head */
	map_bh(bh_result, sb, bno);

brelse_index:
	mark_buffer_dirty(bh_index);
	sync_dirty_buffer(bh_index);
	brelse(bh_index);

	return ret;
}

/*
 * Called by the page cache to read a page from the physical disk and map it in
 * memory.
 */
static int ouichefs_readpage(struct file *file, struct page *page)
{
	return mpage_readpage(page, ouichefs_file_get_block);
}

/*
 * Called by the page cache to write a dirty page to the physical disk (when
 * sync is called or when memory is needed).
 */
static int ouichefs_writepage(struct page *page, struct writeback_control *wbc)
{
	return block_write_full_page(page, ouichefs_file_get_block, wbc);
}

/*
 * Called by the VFS when a write() syscall occurs on file before writing the
 * data in the page cache. This functions checks if the write will be able to
 * complete and allocates the necessary blocks through block_write_begin().
 *
 * MODIFICATIONS
 * After checking if there is enouhg space..
 *  - Read the index block and check the prev member,
 *   * if == 0, file creation, set prev to it's own block number
 *   * if != 0, file modification...
 *    - get new block, cast its data to oichfs index block and
 *      set prev member to the previous version index block number
 *    - Read inode blck and set the new index blokc as file index block
 *    - Iterate over the old index blck data blcks, and for each data block
 *      get a new blokc and copy the data.
 *    - Set new data block number to the new index block in the curent index.
 *
 * NOTE. All modified buffers were marked as dirty
 *  and sync to force the wirte to
 * the disk.
 */
static int ouichefs_write_begin(struct file *file,
				struct address_space *mapping, loff_t pos,
				unsigned int len, unsigned int flags,
				struct page **pagep, void **fsdata)
{
	struct inode *inode = file->f_inode;
	struct super_block *sb = file->f_inode->i_sb;
	struct ouichefs_sb_info *sbi = OUICHEFS_SB(file->f_inode->i_sb);
	struct ouichefs_inode_info *ci = OUICHEFS_INODE(file->f_inode);
	struct ouichefs_file_index_block *index, *index_new;
	struct buffer_head *bh_index, *bh_index_new, *bh_file_data_old;
	struct buffer_head *bh_file_data_new, *bh_ino;
	struct ouichefs_inode *cinode;
	int err, bno, i;
	uint32_t nr_allocs = 0;
	uint32_t inode_block = (inode->i_ino / OUICHEFS_INODES_PER_BLOCK) + 1;
	uint32_t inode_shift = inode->i_ino % OUICHEFS_INODES_PER_BLOCK;



	/* Check if the write can be completed (enough space?) */
	if (pos + len > OUICHEFS_MAX_FILESIZE)
		return -ENOSPC;
	nr_allocs = max(pos + len, file->f_inode->i_size) / OUICHEFS_BLOCK_SIZE;
	if (nr_allocs > file->f_inode->i_blocks - 1)
		nr_allocs -= file->f_inode->i_blocks - 1;
	else
		nr_allocs = 0;
	if (nr_allocs > sbi->nr_free_blocks)
		return -ENOSPC;

	/*
	 *Read ouichefs_inode block to set the new
	 *index block and last version
	 */
	bh_ino = sb_bread(sb, inode_block);
	if (!bh_ino)
		err = -EIO;
	cinode = (struct ouichefs_inode *)bh_ino->b_data;
	cinode += inode_shift;

	/* Read index block from disk */
	bh_index = sb_bread(sb, ci->index_block);
	if (!bh_index)
		return -EIO;
	index = (struct ouichefs_file_index_block *)bh_index->b_data;

	/*First Time the file is created*/
	if (index->prev == 0) {
		/*Initialise prev with its own block number*/
		index->prev = ci->index_block;
		cinode->last_index_block = ci->index_block;
	} else {
		/*File is beeing moidified*/

		if (ci->index_block != cinode->last_index_block)
			goto branch_not_permited;

		/* Get a free block for this new inode's index */
		bno = get_free_block(sbi);
		if (!bno)
			err = -ENOSPC;

		/*Get buffer head of new block to set index data*/
		bh_index_new = sb_bread(sb, bno);
		if (!bh_index_new)
			return -EIO;
		index_new = (struct ouichefs_file_index_block *)
			bh_index_new->b_data;

		/*Set previous index block number to the new block index*/
		index_new->prev = ci->index_block;

		/*Set new index blkc as ouichefs_inode_info index_block*/
		ci->index_block = bno;

		/*Set the new index block to ouichefs_inode->index_bloci*/
		cinode->index_block = ci->index_block;
		cinode->last_index_block = ci->index_block;

		/*Update time metada in the inode and ouichefs_inode*/
		inode->i_ctime = inode->i_atime = inode->i_mtime =
			current_time(inode);
		cinode->i_ctime     = inode->i_ctime.tv_sec;
		cinode->i_atime     = inode->i_atime.tv_sec;
		cinode->i_mtime     = inode->i_mtime.tv_sec;

		/*Copy data from old index_block->block to the new ones*/
		i = 0;
		while (index->blocks[i] != 0) { /*Only if there is data block*/

			/*get new block for data*/
			bno = get_free_block(sbi);
			if (!bno) {
				err = -ENOSPC;
				pr_info("[ERROR] Get new block FAILED");
			}

			/*Get buffer head of the new block to set data*/
			bh_file_data_new = sb_bread(sb, bno);
			if (!bh_file_data_new)
				return -EIO;

			/*Get buffer head of the new blokc to get data*/
			bh_file_data_old = sb_bread(sb, index->blocks[i]);
			if (!bh_file_data_old)
				return -EIO;

			/*copy data from old block to new block*/
			memcpy(bh_file_data_new->b_data,
				bh_file_data_old->b_data,
				OUICHEFS_BLOCK_SIZE);

			/*set data block to index blokc*/
			index_new->blocks[i] = bno;

			/*sync new block int othe disk*/
			mark_buffer_dirty(bh_file_data_new);
			sync_dirty_buffer(bh_file_data_new);
			brelse(bh_file_data_new);
			brelse(bh_file_data_old);

			i++; /*new data block*/
		}

		mark_buffer_dirty(bh_index_new);
		sync_dirty_buffer(bh_index_new);
		brelse(bh_index_new);

	}
	mark_buffer_dirty(bh_index);
	sync_dirty_buffer(bh_index);
	brelse(bh_index);

	/*SYNC inode qui a été modifié*/
	mark_inode_dirty(inode);
	write_inode_now(inode, 1);
	mark_buffer_dirty(bh_ino);
	sync_dirty_buffer(bh_ino);
	brelse(bh_ino);

	/* prepare the write */
	err = block_write_begin(mapping, pos, len, flags, pagep,
				ouichefs_file_get_block);


	/* if this failed, reclaim newly allocated blocks */
	if (err < 0) {
		pr_err("%s:%d: newly allocated blocks reclaim not implemented yet\n",
		       __func__, __LINE__);
	}
	return err;

branch_not_permited:
	mark_buffer_dirty(bh_index);
	sync_dirty_buffer(bh_index);
	brelse(bh_index);

	/*SYNC inode qui a été modifié*/
	mark_inode_dirty(inode);
	write_inode_now(inode, 1);
	mark_buffer_dirty(bh_ino);
	sync_dirty_buffer(bh_ino);
	brelse(bh_ino);

	return -EIO;
}

/*
 * Called by the VFS after writing data from a write() syscall to the page
 * cache. This functions updates inode metadata and truncates the file if
 * necessary.
 *
 * MODIFICATIONS
 * call write_inode_now(inode, 1); after marking the inode as dirty to
 *	force write to the disk.
 * Call sync_fs from superblokc ops to syncronise file system meta data
 * Force write on disk of all moidified buffer_heads with sync_dirty_buffer
 */
static int ouichefs_write_end(struct file *file,
	struct address_space *mapping,
	loff_t pos,
	unsigned int len,
	unsigned int copied,
	struct page *page,
	void *fsdata)
{
	int ret;
	struct inode *inode = file->f_inode;
	struct ouichefs_inode_info *ci = OUICHEFS_INODE(inode);
	struct super_block *sb = inode->i_sb;
	uint32_t nr_blocks_old;

	/* Complete the write() */
	ret = generic_write_end(file, mapping, pos, len, copied, page, fsdata);
	if (ret < len) {
		pr_err("%s:%d: wrote less than asked... what do I do? nothing for now...\n",
		       __func__, __LINE__);
	} else {
		nr_blocks_old = inode->i_blocks;

		/* Update inode metadata */
		inode->i_blocks = inode->i_size / OUICHEFS_BLOCK_SIZE + 2;
		inode->i_mtime = inode->i_ctime = current_time(inode);
		mark_inode_dirty(inode);

		/*Forcer l'écritrue des information de l'inode*/
		write_inode_now(inode, 1);

		sb->s_op->sync_fs(sb, 1);

		/* If file is smaller than before, free unused blocks */
		if (nr_blocks_old > inode->i_blocks) {
			int i;
			struct buffer_head *bh_index;
			struct ouichefs_file_index_block *index;

			/* Free unused blocks from page cache */
			truncate_pagecache(inode, inode->i_size);

			/* Read index block to remove unused blocks */
			bh_index = sb_bread(sb, ci->index_block);
			if (!bh_index) {
				pr_err("failed truncating '%s'. we just lost %llu blocks\n",
				       file->f_path.dentry->d_name.name,
				       nr_blocks_old - inode->i_blocks);
				goto end;
			}
			index = (struct ouichefs_file_index_block *)
				bh_index->b_data;

			for (i = inode->i_blocks - 1; i < nr_blocks_old - 1;
			     i++) {
				put_block(OUICHEFS_SB(sb), index->blocks[i]);
				index->blocks[i] = 0;
			}
			mark_buffer_dirty(bh_index);
			sync_dirty_buffer(bh_index);
			brelse(bh_index);
		}
	}

end:
	//flush_tlb_all();
	flush_cache_all();
	flush_dcache_page(page);
	//syncfs();


	//drop_pagecache_sb();
	return ret;
}

/**
 * @brief Function to change the curent version of a file
 * function called when a CHANGE_VER command is recived
 * with the syscall ioctl.
 *
 * the function gets the file structure thanks to the file descriptor
 * of the file to be changed. This file descriptor is passed as argument
 * with the ioctl syscall
 *
 * The function iterates over the different index_block of each version
 * to find the resquested one. The iteration start ALWAYS from the last
 * index_blokc version thanks to the member last_index_block in the
 * ouichefs_inode structure.
 *
 * The argument version passed as an argument represent the shift of version
 * to do; if version = 0, we go to the last version, if version = 3, we go
 * to the 3th version in the histoty starting by the last one. If there is not
 * enough version, we consder that the requested version doesn't exists. This
 * is detecte by checking the prev member of the current block, if the current
 * index_block is equalt to its prev member, the we get to the end of the
 * histoty.
 */
static int ouichfs_ioctl_change_ver(int fd, int version)
{
	struct file *file;
	struct inode *inode;
	struct super_block *sb;
	struct ouichefs_inode_info *ci;
	struct buffer_head *bh_ino, *bh_index, *bh_cur;
	struct ouichefs_inode *cinode = NULL;
	struct ouichefs_file_index_block *index;
	uint32_t inode_block;
	uint32_t inode_shift;
	int cur_block_num = 0, num_versions = 0, found = 0;

	/*Get file structure for the given fd*/
	file = fget_raw(fd);
	inode = file->f_inode;
	sb = inode->i_sb;

	/*get ouichefs_inode_info*/
	ci = OUICHEFS_INODE(inode);

	/*calcula the block for the ouichefs_inode*/
	inode_block = (inode->i_ino / OUICHEFS_INODES_PER_BLOCK) + 1;
	inode_shift = inode->i_ino % OUICHEFS_INODES_PER_BLOCK;

	/*Read and cast to ouichefs_inode block*/
	bh_ino = sb_bread(inode->i_sb, inode_block);
	if (!bh_ino)
		return -EIO;
	cinode = (struct ouichefs_inode *)bh_ino->b_data;
	cinode += inode_shift;

	/******************************************************
	 * Iterate over index blocks to finde the corresponding
	 * version offset.
	 *****************************************************/

	/*Start iteration ALWAYS from last version*/
	cur_block_num = cinode->last_index_block;

	/**
	 * @brief while stop condition is when index_block->prev point
	 * to its own block number.
	 */
	while (1) {
		/*check if the version requested is the curent one*/
		if (num_versions == version) {
			/*Version Change to the curent version*/
			ci->index_block = (uint32_t)cur_block_num;
			cinode->index_block = ci->index_block;
			found = 1;
			goto iterate_version_end;
		}

		/*Read curent index_block version and cast data*/
		bh_cur = sb_bread(sb, cur_block_num);
		if (!bh_cur)
			return -EIO;
		index = (struct ouichefs_file_index_block *)bh_cur->b_data;

		num_versions++;

		/*chek if we are at the end or pass to previous index block*/
		if (cur_block_num == index->prev)
			goto iterate_version_end;
		else
			cur_block_num = index->prev;

		brelse(bh_cur);
	}

iterate_version_end:

	mark_buffer_dirty(bh_ino);
	sync_dirty_buffer(bh_ino);
	brelse(bh_ino);
	brelse(bh_index);

	/*invalidate cahce if version will be changed*/
	if (found)
		invalidate_mapping_pages(inode->i_mapping, 0, -1);
	else
		pr_info("ERROR, version requested not existant\n");

	fput(file);

	return found;
}

int ouichefs_ioctl_set_last(int fd)
{
	struct file *file;
	struct inode *inode;
	struct super_block *sb;
	struct ouichefs_inode_info *ci;
	struct buffer_head *bh_ino, *bh_cur, *bh_data;
	struct ouichefs_inode *cinode;
	struct ouichefs_file_index_block *file_block;
	struct ouichefs_sb_info *sbi;

	uint32_t inode_block;
	uint32_t inode_shift;

	int cur_block_num, tmp_block_num, i;

	/*Get file structure for the given fd*/
	file = fget_raw(fd);
	inode = file->f_inode;
	sb = inode->i_sb;
	sbi = OUICHEFS_SB(sb);

	/*get ouichefs_inode_info*/
	ci = OUICHEFS_INODE(inode);

	/*calcula the block for the ouichefs_inode*/
	inode_block = (inode->i_ino / OUICHEFS_INODES_PER_BLOCK) + 1;
	inode_shift = inode->i_ino % OUICHEFS_INODES_PER_BLOCK;

	/*Read and cast to ouichefs_inode block*/
	bh_ino = sb_bread(inode->i_sb, inode_block);
	if (!bh_ino)
		return -EIO;
	cinode = (struct ouichefs_inode *)bh_ino->b_data;
	cinode += inode_shift;

	/*Start iteration ALWAYS from last version*/
	cur_block_num = cinode->last_index_block;

	/**
	 * @brief while stop condition is when index_block->prev point
	 * to its own block number.
	 */
	while (1) {

		/*Is the curent file index_block the last_file_index_blokc?*/
		if (ci->index_block == cur_block_num)
			goto iterate_version_end;

		/*Iterate and free data blokc*/
		/*
		* Cleanup pointed blocks if unlinking a file. if read
		* index block fails, cleanup inode anyway and lose this
		* file's blocks forever. If we fail to scrub a data block,
		* don't fail(too late anyway), just put the block and continue.
		*/
		bh_cur = sb_bread(sb, cur_block_num);
		if (!bh_cur)
			return -EIO;
		file_block = (struct ouichefs_file_index_block *)bh_cur->b_data;

		for (i = 0; i < inode->i_blocks - 1; i++) {
			char *block;

			if (!file_block->blocks[i])
				continue;

			put_block(sbi, file_block->blocks[i]);
			bh_data = sb_bread(sb, file_block->blocks[i]);
			if (!bh_data)
				continue;
			block = (char *)bh_data->b_data;
			memset(block, 0, OUICHEFS_BLOCK_SIZE);
			mark_buffer_dirty(bh_data);
			brelse(bh_data);
		}

		/*set prev index_block to check*/
		tmp_block_num = cur_block_num;
		cur_block_num = file_block->prev;

		/* Scrub index block */
		put_block(sbi, tmp_block_num);
		memset(file_block, 0, OUICHEFS_BLOCK_SIZE);
		mark_buffer_dirty(bh_cur);
		brelse(bh_cur);
	}

iterate_version_end:

	cinode->last_index_block = ci->index_block;

	mark_buffer_dirty(bh_ino);
	sync_dirty_buffer(bh_ino);
	brelse(bh_ino);

	/*invalidate cahce if version will be changed*/
	invalidate_mapping_pages(inode->i_mapping, 0, -1);

	fput(file);

	return 0;
}

/**
 * @brief Function call when a ioctl syscall is executed.
 * Test the magic number and command
 *
 * @param filep
 * @param cmd -> command
 * @param arg -> argument. To be casted to the corresponding data command
 * @return long
 */
static long ouichfs_ioctl_func(struct file *filep,
	unsigned int cmd,
	unsigned long arg)
{
	struct ioctl_arg data;
	int ret;

	if (_IOC_TYPE(cmd) != IOC_MAGIC)
		return -ENOTTY;

	if (cmd == CHANGE_VER) {
		ret = copy_from_user(&data,
			(struct ioctl_arg *)arg,
			sizeof(struct ioctl_arg));
		if (ret != 0)
			pr_info("[ouichfs_ioctl_func] copy_from_user lost %d b"
				, ret);
		ouichfs_ioctl_change_ver(data.fd, data.version);
	} else if (cmd == SET_LAST) {
		ouichefs_ioctl_set_last(arg);
	}

	return 0;
}

const struct address_space_operations ouichefs_aops = {
	.readpage    = ouichefs_readpage,
	.writepage   = ouichefs_writepage,
	.write_begin = ouichefs_write_begin,
	.write_end   = ouichefs_write_end
};

const struct file_operations ouichefs_file_ops = {
	.owner      = THIS_MODULE,
	.llseek     = generic_file_llseek,
	.read_iter  = generic_file_read_iter,
	.write_iter = generic_file_write_iter,
	.unlocked_ioctl = ouichfs_ioctl_func
};
