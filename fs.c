// SPDX-License-Identifier: GPL-2.0
/*
 * ouiche_fs - a simple educational filesystem for Linux
 *
 * Copyright (C) 2018  Redha Gouicem <redha.gouicem@lip6.fr>
 */

#define pr_fmt(fmt) KBUILD_MODNAME ": " fmt

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/debugfs.h>
#include <linux/path.h>
#include <linux/namei.h>
#include <linux/buffer_head.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/ioctl.h>

#include "ouichefs.h"
#include "bitmap.h"

static unsigned long inode_num;
static struct super_block *super_block_root;

/*ioctl driver variable*/
dev_t ouichefs_dev = 0;
static struct class *ouichefs_dev_class;
static struct cdev ouichefs_etx_cdev;

/*
 * Mount a ouiche_fs partition
 */
struct dentry *ouichefs_mount(struct file_system_type *fs_type,
							int flags,
							const char *dev_name,
							void *data)
{
	struct dentry *dentry = NULL;

	dentry = mount_bdev(fs_type, flags, dev_name, data,
			    ouichefs_fill_super);
	if (IS_ERR(dentry))
		pr_err("'%s' mount failure\n", dev_name);
	else
		pr_info("'%s' mount success\n", dev_name);

	inode_num = dentry->d_inode->i_ino;
	super_block_root = dentry->d_inode->i_sb;

	return dentry;
}

/*
 * Unmount a ouiche_fs partition
 */
void ouichefs_kill_sb(struct super_block *sb)
{
	kill_block_super(sb);

	pr_info("unmounted disk\n");
}

static struct file_system_type ouichefs_file_system_type = {
	.owner = THIS_MODULE,
	.name = "ouichefs",
	.mount = ouichefs_mount,
	.kill_sb = ouichefs_kill_sb,
	.fs_flags = FS_REQUIRES_DEV,
	.next = NULL,
};

/**
 * @brief UNUSED
 *
 * function debugfs to write
 *
 */
static ssize_t ouichfs_debugfs_write(struct file *filp,
	const char __user *buf,
	size_t count,
	loff_t *ppos)
{
	int val = 0;
	char temp[10];
	struct inode *inode;
	struct buffer_head *bh_ino;
	struct ouichefs_inode *cinode = NULL;
	char path_name[10];
	struct ouichefs_inode_info *ci;
	uint32_t inode_block;
	uint32_t inode_shift;

	if (copy_from_user(temp, buf, count))
		pr_info("copy_from_user not all bytes not copied");
	temp[count] = 10;
	temp[count + 1] = 0;
	if (kstrtoint(temp, 10, &val) != 0)
		pr_info("ERROR kstrtoint");

	inode = ouichefs_iget(super_block_root, inode_num);

	ci = OUICHEFS_INODE(inode);

	ci->index_block = (uint32_t)val;

	inode_block = (inode->i_ino / OUICHEFS_INODES_PER_BLOCK) + 1;
	inode_shift = inode->i_ino % OUICHEFS_INODES_PER_BLOCK;

	bh_ino = sb_bread(inode->i_sb, inode_block);
	if (!bh_ino)
		return -EIO;
	cinode = (struct ouichefs_inode *)bh_ino->b_data;
	cinode += inode_shift;
	cinode->index_block = ci->index_block;

	mark_buffer_dirty(bh_ino);
	sync_dirty_buffer(bh_ino);
	brelse(bh_ino);

	invalidate_mapping_pages(inode->i_mapping, 0, -1);

	return 0;
}

void show_hb_block_data(struct seq_file *output, struct super_block *sb, struct ouichefs_file_index_block *index_block, int block_no)
{
	struct buffer_head *bh_data;
	int i = 0;

	while (index_block->blocks[i] != 0) {
		bh_data = sb_bread(sb, index_block->blocks[i]);
		if (!bh_data)
			return;

		seq_printf(output, "Data in blokcNo:%d:%s", index_block->blocks[i], bh_data->b_data);

		brelse(bh_data);
		i++;
	}
}

/**
 * @brief called when read over debugfs file
 * it is called when the functioon is_directory() find a file inode type
 *
 * this function itérates over the versions of the curent file and prints
 * the date with seq_print()
 *
 * @param inode -> inode of the file
 * @param output  -> seq_file structure to print
 * @return int
 */
int is_file(struct inode *inode, struct seq_file *output, char *filename)
{
	struct super_block *sb = inode->i_sb;
	struct ouichefs_inode_info *ci = OUICHEFS_INODE(inode);
	struct ouichefs_file_index_block *index;
	struct buffer_head *bh_index, *bh_cur, *bh_ino;
	struct ouichefs_inode *cinode;
	uint32_t cur_block_num;
	int num_versions = 0, temp_count = 0;
	uint32_t inode_block = (inode->i_ino / OUICHEFS_INODES_PER_BLOCK) + 1;
	uint32_t inode_shift = inode->i_ino % OUICHEFS_INODES_PER_BLOCK;

	char temp[1024];

	memset(temp, 0, 1024);

	seq_printf(output, "%s\t", filename);
	seq_printf(output, "ino: %lu\t", inode->i_ino);

	bh_index = sb_bread(sb, ci->index_block);
	if (!bh_index)
		return -EIO;
	index = (struct ouichefs_file_index_block *)bh_index->b_data;

	/*read inode to get last version*/
	bh_ino = sb_bread(sb, inode_block);
	if (!bh_ino)
		return -EIO;
	cinode = (struct ouichefs_inode *)bh_ino->b_data;
	cinode += inode_shift;

	cur_block_num = cinode->last_index_block;

	/**
	 * @brief while stop condition is when index_block->prev point
	 * to its own block number.
	 */
	while (1) {
		bh_cur = sb_bread(sb, cur_block_num);
		if (!bh_cur)
			return -EIO;
		index = (struct ouichefs_file_index_block *)bh_cur->b_data;

		temp_count += sprintf(temp, "%s%u, ", temp, cur_block_num);

		show_hb_block_data(output, sb, index, cur_block_num);

		num_versions++;

		if (cur_block_num == index->prev) {
			brelse(bh_cur);
			break;
		}
			

		cur_block_num = index->prev;

		brelse(bh_cur);
	}

	seq_printf(output, "nb_ver: %d\tcur_ver: %u\tindex_blocks: %s\n",
		num_versions,
		ci->index_block,
		temp);

	brelse(bh_index);
	brelse(bh_ino);

	return 0;
}

/**
 * @brief is directory iterates all block in its index_block,
 * and checks if it is a directory or a file
 *  - if file, call is_file() to show information in the console
 *  - if directory, call is_directory() recursively to itérate all
 *    sub directories
 *
 * @param inode -> inode of the directory
 * @param output  -> seq_file structure to print
 */
int is_directory(struct inode *inode, struct seq_file *output)
{
	struct inode *curent_inode;
	struct super_block *sb = inode->i_sb;
	struct ouichefs_inode_info *ci = OUICHEFS_INODE(inode);
	struct buffer_head *bh_root, *bh_inode;
	struct ouichefs_inode *cinode;
	struct ouichefs_file *oui_file = NULL;
	struct ouichefs_dir_block *dir_block = NULL;
	int i;
	uint32_t inode_block;
	uint32_t inode_shift;

	/* Check that dir is a directory */
	if (!S_ISDIR(inode->i_mode))
		return -ENOTDIR;

	/*Read index block and get dir_blck*/
	bh_root = sb_bread(sb, ci->index_block);
	if (!bh_root)
		return -EIO;
	dir_block = (struct ouichefs_dir_block *)bh_root->b_data;

	/**
	 * @brief Iterate over the index block
	 * Get inode and identify if it is a file or a directory
	 * file			-> call is_file to show file history
	 * directory	-> call is_directory recursively
	 */
	for (i = 0; i < OUICHEFS_MAX_SUBFILES; i++) {
		oui_file = &dir_block->files[i];
		if (!oui_file->inode)
			continue;

		inode_block = (oui_file->inode / OUICHEFS_INODES_PER_BLOCK) + 1;
		inode_shift = oui_file->inode % OUICHEFS_INODES_PER_BLOCK;

		bh_inode = sb_bread(sb, inode_block);
		if (!bh_inode)
			return -EIO;
		cinode = (struct ouichefs_inode *)bh_inode->b_data;
		cinode += inode_shift;

		curent_inode = ouichefs_iget(sb, oui_file->inode);

		if (S_ISDIR(curent_inode->i_mode))
			is_directory(curent_inode, output);
		else if (S_ISREG(curent_inode->i_mode))
			is_file(curent_inode, output, oui_file->filename);

		brelse(bh_inode);
	}

	brelse(bh_root);

	return 0;
}

/**
 * @brief Function call when a read acces to the debugfs file
 * this function get the inode of the root file of the ouichefs
 * partition and the call is_directory() to iterate every file
 * in the partition
 *
 * @param s -> seq_file structure. To be used with seq_print
 * @param data -> unused
 */
static int ouichfs_debugfs_show(struct seq_file *s, void *data)
{
	struct inode *inode;

	/**
	 * @brief Get root inode
	 * super_block_roo and inode_num are store in global variables
	 * at the execution of mount function.
	 */
	inode = ouichefs_iget(super_block_root, inode_num);

	/*Root is always a directory*/
	is_directory(inode, s);

	return 0;
}

/**
 * @brief Wrapper of open syscall()
 */
int ouichfs_debugds_open(struct inode *inode, struct file *file)
{
	return single_open(file, ouichfs_debugfs_show, inode->i_private);
}

const static struct file_operations ouichefs_debugfs_fops = {
	.owner = THIS_MODULE,
	.read  = seq_read,
	.write = ouichfs_debugfs_write,
	.open =	ouichfs_debugds_open,
	.llseek = seq_lseek,
	.release = single_release
};

static struct dentry *ouichefs_debug_dir;

static int __init ouichefs_init(void)
{
	int ret;
	struct dentry *new_file;

	ret = ouichefs_init_inode_cache();
	if (ret) {
		pr_err("inode cache creation failed\n");
		goto end;
	}

	ret = register_filesystem(&ouichefs_file_system_type);
	if (ret) {
		pr_err("register_filesystem() failed\n");
		goto end;
	}

	/*debugds directory in /sys/kernel/debug*/
	ouichefs_debug_dir = debugfs_create_dir("ouichefs_partition", NULL);
	if (ouichefs_debug_dir == NULL)
		return -ENOTDIR;

	/*debugfs file in /sys/kernel/debug/ouichefs_debug/*/
	new_file = debugfs_create_file("debug_partition",
		0777,
		ouichefs_debug_dir,
		NULL,
		&ouichefs_debugfs_fops);
	if (!new_file)
		goto remove_debugfs;

	pr_info("module loaded\n");
	return ret;

remove_debugfs:
	debugfs_remove_recursive(ouichefs_debug_dir);

end:
	return ret;
}

static void __exit ouichefs_exit(void)
{
	int ret;

	ret = unregister_filesystem(&ouichefs_file_system_type);
	if (ret)
		pr_err("unregister_filesystem() failed\n");

	ouichefs_destroy_inode_cache();

	pr_info("module unloaded\n");
}

module_init(ouichefs_init);
module_exit(ouichefs_exit);


MODULE_LICENSE("GPL");
MODULE_AUTHOR("Redha Gouicem, <redha.gouicem@lip6.fr>");
MODULE_DESCRIPTION("ouichefs, a simple educational filesystem for Linux");
