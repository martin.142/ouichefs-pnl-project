#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>

#define IOC_MAGIC 'C'
#define SET_LAST	_IOW(IOC_MAGIC, 1, int)

/**
 * @brief this file test all new functions from
 * file systeme
 *
 * @return int
 */

int main(int argc, char **argv)
{
	int fd_file, ver_offset;
	char temp[10];

	/*Test arguments. file name*/
	if (argc < 2) {
		printf("A file and a version offsert must bee specified\n");
		return 0;
	}

	fd_file = open(argv[1], 0, O_RDWR);
	if (fd_file < 0) {
		printf("Error, file not found\n");
		return 0;
	}

	if (ioctl(fd_file, SET_LAST, fd_file) != 0)
		printf("ERROR, ioctl(HELLO) failed\n");

	close(fd_file);

	return 0;
}
