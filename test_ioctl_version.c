#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>

#define IOC_MAGIC 'C'
#define CHANGE_VER _IOW(IOC_MAGIC, 0, struct ioctl_arg *)
struct ioctl_arg {
	int fd;
	int version;
};

/**
 * @brief this file test all new functions from
 * file systeme
 * @return int
 */

int main(int argc, char **argv)
{
	int fd_file, fd_ioctl, ver_offset;
	char temp[10];
	struct ioctl_arg data;

	/*Test arguments. file name and version offset*/
	if (argc < 3) {
		printf("A file and a version offsert must bee specified\n");
		return 0;
	}


	fd_file = open(argv[1], 0, O_RDWR);
	if (fd_file < 0) {
		printf("Error, file not found\n");
		return 0;
	}

	ver_offset = atoi(argv[2]);
	if (ver_offset < 0) {
		printf("Error, version offet not valid\n");
		return 0;
	}

	data.fd = fd_file;
	data.version = ver_offset;

	if (ioctl(fd_file, CHANGE_VER, &data) != 0)
		printf("ERROR, ioctl(HELLO) failed\n");

	close(fd_file);

	return 0;
}
